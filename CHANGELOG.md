## [2.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-best-practises/compare/v2.0.1...v2.0.2) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([495d2af](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-best-practises/commit/495d2af9fcf771ad0c00bf86c6d4a0511f975067))

## [2.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-best-practises/compare/v2.0.0...v2.0.1) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([2b82902](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-best-practises/commit/2b8290265db63ec4eb3f2883c5478972ef4159af))

# [2.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-best-practises/compare/v1.0.1...v2.0.0) (2023-10-08)


### Features

* **opendesk:** Add additional components. ([f5d81ca](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-best-practises/commit/f5d81caae3232c20d6d7626cbb4ee9d6f346af6f))


### BREAKING CHANGES

* **opendesk:** Refactor commonLabels into additionalLabels.

## [1.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-best-practises/compare/v1.0.0...v1.0.1) (2023-10-04)


### Bug Fixes

* **opendesk:** Use external-registry oci mirror of common chart ([58a6ba4](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-best-practises/commit/58a6ba426705eb70bff1a7f0eec7b806e78592c2))

# 1.0.0 (2023-10-01)


### Features

* **opendesk:** Add initial opendesk chart ([c64e29e](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-best-practises/commit/c64e29ee06a018ac822930dded91df6ee882c066))

<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
